package calculator;

public class Calculator {

    public static void main(String[] args) {
        Operations ops = new Operations();
        System.out.println("1 + 2 = " + ops.add(1, 2));
        System.out.println("2 * 3 = " + ops.multiply(2, 3));
    }

}